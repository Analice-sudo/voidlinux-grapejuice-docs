# Void Linux

# Enabling 32-bit Repos
```sh
xbps-install -y void-repo-nonfree void-repo-multilib void-repo-multilib-nonfree
```
Note to also update the package database:
```sh
xbps-install -Sy
```

# Installing Wine
```sh
xbps-install wine wine-devel wine-32bit
```
# Installing Grapejuice

## Installing Grapejuice dependencies
```sh
xbps-install git python3-devel python-devel python3-pip python3-cairo gtk+3 gobject-introspection desktop-file-utils xdg-utils xdg-user-dirs gtk-update-icon-cache shared-mime-info
```

## Clone the git repository
```sh
git clone https://gitlab.com/brinkervii/grapejuice.git /tmp/grapejuice
```

## Run the install script
```sh
cd /tmp/grapejuice
./install.py
```

WIP
